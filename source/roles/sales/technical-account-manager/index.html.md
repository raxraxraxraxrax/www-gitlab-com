---
layout: job_page
title: "Technical Account Manager"
---

Are you passionate about customer success?  Do you have a proven customer acumen with a solid technical foundation? Then come be a member of GitLab's Customer Success Team. Providing guidance, planning and oversight while leveraging adoption and technical best practices. The Technical Account Manager is the key partner helping customers achieve their strategic objectives and maximum value from their investment in GitLab.  Additionally, the TAM serves as the liaison between the customer and the GitLab ecosystem, streamlining collaboration with Product Management, Engineering, Sales, Professional Services and others.

To learn more, see the [Technical Account Manager handbook](/handbook/customer-success/tam)

### Responsibilities

- Provide immediate on-boarding activities such as installation and training following investment of GitLab
- Own overall relationship with assigned clients, which include: increasing adoption, ensuring retention, and satisfaction
- Work with clients to build Customer Success Plans, establishing critical goals, or other key performance indicators and aid the customer in achieving their goals
- Measure and monitor customers achievement of critical and key performance indicators, reporting both internally to GitLab account stakeholders and externally to Customer Sponsors and Executives
- Establish regular cadence (weekly, Monthly, Quarterly) with each assigned clients, to review health metrics
- Establish a trusted/strategic advisor relationship with each assigned client and drive continued value of our solution and services
- Work closely with the GitLab Sales Account team (Account Executive, Solutions Architects, Professional Services) to identify opportunities for new usage of GitLab across organizational functions
- Work to identify and/or develop up-sell opportunities
- Advocate customer needs/issues cross-departmentally
- Program manage account escalations
- Assist and provide expert deployment, operational best practices and establishing a GitLab Center of Excellence
- Assist in workshops to help customers leverage the full value of GitLab solution
- Provide insights with respect to the availability and applicability of new features in GitLab
- Support GitLab Services in identifying and recommending training opportunities
- Act as the GitLab liaison for GitLab technical questions, issues or escalations.  This will include working with GitLab Support, Product Management(i.e. roadmaps), or others needed
- Maintain current functional knowledge and technical knowledge of GitLab platform

### Requirements

- 7 + years of experience in a related function is required with direct customer advocacy and engagement experience in post-sales or professional services functions
- Prior experience in Customer Success or equivalent history of increasing satisfaction, adoption, and retention
- Familiarity working with clients of all sizes, especially large enterprise organizations
- Exception verbal, written, organizational, presentation, and communications skills
- Detailed oriented and analytical
- Strong team player but self starter
- Strong technical, analytic and problem solving skills
- Experience with Ruby on Rails applications and Git
- Deep knowledge of software development lifecycle and development pipeline
- Understanding of continuous integration, continuous deployment, chatOps, and cloud native
- Above average knowledge of Unix and Unix based Operating Systems
- Installation and operation of Linux operating systems and hardware investigation/manipulation commands
- BASH/Shell scripting including systems and init.d startup scripts
- Package management (RPM, etc. to add/remove/list packages)
- Understanding of system log files/logging infrastructure
- B.Sc. in Computer Science or equivalent experience
- Programming/scripting experience & skill is required (Bash & Ruby)
- Project management experience & skills
- SCM admin and/or PS experience would be a plus
- Set up HA/DR, working with Containers and Schedulers (Kubernetes preferred) and also experience with AWS stack (EC2, ECS, RDS, ElastiCache)

## Manager of Customer Experience 

The Manager of Customer Experience is a management position on the front line working with the Technical Account Managers and Account Managers helping evolve and grow our large and strategic customers. This is a player/coach role where the individual is expected to be experienced in and have advanced insight to the GitLab platform. The individual will contribute to territory and account strategy as well as driving the execution directly and indirectly. The individual will need to be very comfortable giving and receiving positive and constructive feedback, as well as adapting to environmental change and retrospecting on successes and failures. The Manager of Customer Experience will work together with the other managers within the customer success organization to help execute on strategies and vision with the Director.   

### Responsibilities

- Work with the Director of Customer Success to help establish and manage goals and responsibilities for Implementation Engineers
- Assist in development of thought leadership, event-specific, and customer-facing presentations
- Share hands-on technical preparation and presentation work for key accounts
- Ensure the TAMS  exceeds corporate expectations in core knowledge, communication, and execution
- Define and maintain a high bar for team member expectations and enable the team to achieve it
- Challenge the team and yourself to continually learn and grow as trusted advisors to clients
- Monitor performance of team members and provide timely feedback and development assistance
- Create, review, and approve formal statements of work, change requests, and proposals
- Prepare weekly revenue forecast worksheet and create action plans to address issues
- Develop senior-level relationships with customers
- Manage resource assignments and staffing levels, including recruitment as needed
- Identify and implement improvements to the processes and tools used as a seasoned with experience leading teams of project managers and consultants in support of external customers
- Work together with our sales organization to propose, scope, and price professional services statements of work
- Work together with Solutions Architects and Implementation Engineers to plan and execute internal projects, ensure that teams have appropriate training and manage resources to deliver Customer Success offerings
- Provide leadership and guidance to coach, motivate, and lead
- Services team members to their optimum performance levels and career development
- Ensure delivery model is focused on quality, cost effective delivery of services, and customer success outcomes
- Remains knowledgeable and up-to-date on GitLab releases
- Documents services provided to customers, including new code, techniques and processes, in such a way as to make such services more efficient in the future and to add to the GitLab community
- Works with the product engineering and support teams to contribute documentation for GitLab
- Help build programs that the TAMs will execute to effectively grow our enterprise customers 
- Ensure your team maintains high levels of customer retention, customer satisfaction, and customer expansion as measured by quarterly goals, surveys, and NPS
- Partner with technical support leadership to ensure a seamless customer handoff of escalated technical issues
- Develop strong relationships with engineering and product to ensure cohesion and shared understanding of goals and initiatives
- Drive team to exceed company growth and retention forecasts
- Oversee initiatives set forth in quarterly goals
- Manage a team of highly motivated, customer-focused Technical Account Managers to manage the overall health and care of accounts (e.g. onboarding customers, product/service adoption by user, monitoring engagement across the post-sale lifecycle), and develop compensation plans and career paths appropriate to specific functions within the team

### Requirements 

- 7+ years of experience in a related function is required with direct customer advocacy and engagement experience in post-sales or professional services functions
- Prior experience in customer success or equivalent history of increasing satisfaction, adoption, and retention
- Familiarity working with clients of all sizes, especially large enterprise organizations
- Exception verbal, written, organizational, presentation, and communications skills
- Detailed oriented and analytical
- Strong team player but self starter
- Strong technical, analytic and problem solving skills 
- Experience with Ruby on Rails applications and Git
- Deep knowledge of software development lifecycle and development pipeline
- Understanding of continuous integration, continuous deployment, ChatOps, and cloud native
- Above average knowledge of Unix and Unix based operating systems
- Installation and operation of Linux operating systems and hardware investigation/manipulation commands
- BASH/Shell scripting including systems and init.d startup scripts
- Package management (RPM, etc. to add/remove/list packages)
- Understanding of system log files/logging infrastructure
- B.Sc. in Computer Science or equivalent experience
- Programming/scripting experience & skill is required (Bash & Ruby)
- Project management experience & skills
- SCM admin and/or PS experience would be a plus
- Set up HA/DR, working with Containers and Schedulers (Kubernetes preferred) and also experience with AWS stack (EC2, ECS, RDS, ElastiCache)

## Hiring Process

Applicants can expect the hiring process to follow the order below. Please keep in mind that applicants can be declined from the position at any stage of the process. To learn more about someone who may be conducting the interview, find her/his job title on our [team page](/team).

- Qualified applicants receive a short questionnaire.
- Selected candidates will be invited to schedule a 30 minute [screening call](/handbook/hiring/interviewing/#screening-call).
- Candidates will be invited to schedule interviews with Director of Customer Success.
- Next, candidates will be invited to schedule an interview with members of the -Customer Success Team.
- Candidates may be invited to schedule an interview with our CRO.
- Finally, candidates may interview with our CEO.
- Successful candidates will subsequently be made an offer via email.
